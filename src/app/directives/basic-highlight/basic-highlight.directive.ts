import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit {

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    // AVOID TO USE THIS APPROACH ... USE RENDERER2 INSTEAD
    this.elementRef.nativeElement.style.backgroundColor = 'green';
  }

}
